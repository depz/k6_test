import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  stages: [
    { duration: '1m', target: 10 },
    { duration: '1m', target: 20 },
    { duration: '1m', target: 100 },
  ],
  thresholds: { http_req_duration: ['avg<100', 'p(95)<200'] },
  noConnectionReuse: true,
};

export default function () {
  http.get(`http://productpage.${__ENV.MY_NAMESPACE}.svc.cluster.local:9080/api/vi/products`);
  sleep(1);
}
